import os.path
import sqlite3
import time


def tabellaExist(tabella, database):
    connettiDatabase = sqlite3.connect(database)
    cursore = connettiDatabase.cursor()
    print ("Controllo se esiste la tabella ", tabella, "...", sep="")
    cursore.execute("SELECT COUNT(*) FROM sqlite_master WHERE type = 'table' AND name = ?;", (tabella,))
    if cursore.fetchone()[0] == 0:
        print("Tabella " + str(tabella) + " assente!")
        return False
    else:
        return True


class Database:


    def adminActions(userIdCandidate, userId, action, database):
        sub = {}
        sub["user"] = {}
        sub["user"]["action"] = action
        sub["candidate"] = {}
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("PRAGMA foreign_keys = ON") #Mi tocca specificarglielo di continuo, altrimenti sqlite fa la cacca e mi esegue la query con il supporto alle chiavi esterne spento >:(
        cursore.execute("SELECT * FROM admins WHERE user_id=?;", (userId,))
        try:
            tmp_list = []
            for tmp in cursore.fetchone():
                tmp_list.append(tmp)
            sub["user"]["user_id"] = tmp_list[0]
            if tmp_list[1]:
                sub["user"]["role"] = "creator"
            else:
                sub["user"]["role"] = "admin"
        except:
            sub["user"]["user_id"] = userId
            sub["user"]["role"] = "peasant"
        cursore.execute("SELECT * FROM admins WHERE user_id=?;", (userIdCandidate,))
        try:
            tmp_list = []
            for tmp in cursore.fetchone():
                tmp_list.append(tmp)
            sub["candidate"]["user_id"] = tmp_list[0]
            if tmp_list[1]:
                sub["candidate"]["role"] = "creator"
            else:
                sub["candidate"]["role"] = "admin"
        except:
            cursore.execute("SELECT user_id FROM users WHERE user_id=?;", (userIdCandidate,))
            try:
                sub["candidate"]["user_id"] = cursore.fetchone()[0]
                sub["candidate"]["role"] = "peasant"
            except:
                sub["candidate"]["user_id"] = userIdCandidate
                sub["candidate"]["role"] = "notInDatabase"
        #print(sub)
        if sub["candidate"]["role"] == "notInDatabase":
            sub["success"] = False
            sub["reason"] = sub["candidate"]["role"]
        elif sub["user"]["role"] == "peasant":
            sub["success"] = False
            sub["reason"] = "noPower"
        else:
            if sub["user"]["action"] == "promote":
                if sub["candidate"]["role"] == "peasant" and sub["user"]["role"] == "creator": #Promote peasant to admin
                    cursore.execute("INSERT INTO admins (user_id, is_creator) VALUES (?, 0);", (sub["candidate"]["user_id"],))
                    sub["success"] = True
                    sub["reason"] = "promotedToAdmin"
                elif sub["candidate"]["role"] == "admin" and sub["user"]["role"] == "creator": #Promote to creator
                    cursore.execute("UPDATE admins SET is_creator = 1 WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                    sub["success"] = True
                    sub["reason"] = "promotedToCreator"
                else: #You cannot promote this candidate more than this!
                    sub["success"] = False
                    sub["reason"] = "cannotPromoteMoreThanThis"
            elif sub["user"]["action"] == "demote":
                if sub["candidate"]["role"] == "admin" and sub["user"]["role"] == "creator": #Demote admin to peasant
                    cursore.execute("DELETE FROM admins WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                    sub["success"] = True
                    sub["reason"] = "demotedToPeasant"
                elif sub["candidate"]["user_id"] == sub["user"]["user_id"]:
                    if sub["user"]["role"] == "admin": #Demote yourself from admin to peasant
                        cursore.execute("DELETE FROM admins WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                        sub["success"] = True
                        sub["reason"] = "demotedToPeasant"
                    else:
                        #Else you are a creator
                        #Demote yourself from creator to admin only if you're not the only one creator
                        cursore.execute("SELECT count(*) FROM admins WHERE is_creator = 1;")
                        numberCreators = int(cursore.fetchone()[0])
                        if numberCreators > 1:
                            cursore.execute("UPDATE admins SET is_creator = 0 WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                            sub["success"] = True
                            sub["reason"] = "demotedToAdmin"
                        else:
                            sub["success"] = False
                            sub["reason"] = "onlyOneCreator"
                else:
                    sub["success"] = False
                    sub["reason"] = "cannotDemoteMoreThanThis"
            elif sub["user"]["action"] == "kick":
                if sub["candidate"]["role"] == "peasant": #Kick peasant
                    cursore.execute("DELETE FROM users WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                    sub["success"] = True
                    sub["reason"] = "kickedPeasant"
                elif sub["candidate"]["role"] == "admin" and sub["user"]["role"] == "creator": #Kick admin
                    cursore.execute("DELETE FROM users WHERE user_id = ?;", (sub["candidate"]["user_id"],))
                    sub["success"] = True
                    sub["reason"] = "kickedAdmin"
                else:
                    #You don't have the privileges to kick this user
                    sub["success"] = False
                    sub["reason"] = "noPowerToKick"
            else:
                sub["success"] = False
                sub["reason"] = "unknownCommand"
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()
        return sub



    def isAdminOrCreator(chatId, database):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT count(user_id) FROM admins WHERE user_id=?;", (chatId,))
        if cursore.fetchone()[0]:
            exists = True
        else:
            exists = False
        cursore.close()
        connettiDatabase.close()
        return exists

    def databaseExist(database):
        
        print ("Versione modulo:", sqlite3.version)
        print ("Versione libreria SQLite:", sqlite3.sqlite_version)
        print ("Nome database SQLite:", database)

        print ("Controllo se esiste già un file chiamato \"" + database + "\"...")
        if os.path.isfile(database):
            print ("Il file ESISTE")
            return True
        else:
            print ("Il file NON esiste")
            return False


    def tabelleExist(tabelle, database):
        for tabella in tabelle:
            if not tabellaExist(tabella, database):
                return False
        return True


    def creators_number(database):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT count(user_id) FROM admins WHERE is_creator = 1;")
        number = cursore.fetchone()[0]
        cursore.close()
        connettiDatabase.close()
        return number

    def subscribe_and_promote_creator(database, user_creator):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("INSERT INTO users (user_id, username, chat_id, notifications, max_registrations, subscription_time) VALUES (?, ?, ?, ?, ?, ?);", (user_creator["user_id"], user_creator["username"],user_creator["chat_id"],1, 1000, int(time.time())))
        cursore.execute("INSERT INTO admins (user_id, is_creator) VALUES (?, 1);", (user_creator["user_id"],))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()

    def eseguiQueryNoReturn(database, query):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        #print(query)
        cursore.execute(query)
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def checkUtente(database, tabella, userId, mode):
        userId = str(userId)
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        if mode == "usernameExists":
            query = ("SELECT 1 FROM "+tabella+" WHERE username = ?;")
        if mode == "userIdExists":
            query = ("SELECT 1 FROM "+tabella+" WHERE user_id = ?;")
        elif mode == "exists":
            query = ("SELECT 1 FROM "+tabella+" WHERE user_id = ?;")
        elif mode == "isStart":
            query = ("SELECT isStart FROM "+tabella+" WHERE user_id = ?;")
        elif mode == "isNotifications":
            query = ("SELECT notifications FROM "+tabella+" WHERE user_id = ?;")
        elif mode == "isAdmin":
            query = ("SELECT 1 FROM "+tabella+" WHERE userid = ?;")
        #print(query)
        cursore.execute(query, (userId,))
        try:
            if cursore.fetchone()[0]:
                exists = True
            else:
                exists = False
        except:
            exists = False
        cursore.close()
        connettiDatabase.close()
        return exists


    def get_first_social_id_from_internal_user_social_and_if_present_subscribe(database, userId, sub, existsForSure):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        if sub["internal_id"]:
            cursore.execute("SELECT social_id, username, title, status FROM socials WHERE social = ? AND internal_id = ?;", (sub["social"],sub["internal_id"],))
        elif sub["username"]:
            cursore.execute("SELECT social_id, internal_id, title, status FROM socials WHERE social = ? AND username = ?;", (sub["social"],sub["username"],))
        else:
            cursore.close()
            connettiDatabase.close()
            sub["subStatus"] = "internalId_or_username_not_present"
            return sub #Errore, internal_id od username non presenti
        try:
            #Ottengo il social_id ed i restanti dati mancanti
            tmp_list = []
            for tmp in cursore.fetchone():
                tmp_list.append(tmp)
            sub["social_id"] = tmp_list[0]
            if not existsForSure:
                sub["status"] = tmp_list[3]
                sub["title"] = tmp_list[2]
                if sub["internal_id"]:
                    sub["username"] = tmp_list[1]
                elif sub["username"]:
                    sub["internal_id"] = tmp_list[1]
                else:
                    cursore.close()
                    connettiDatabase.close()
                    return -1 #Errore, è praticamente impossibile arrivare in questo else
            else:
                pass
        except:
            #La ricerca ha dato riscontro negativo, non si trova nel database
            if existsForSure:
                cursore.execute("INSERT INTO socials (social, username, title, internal_id, retreive_time) VALUES (?, ?, ?, ?, ?);", (sub["social"],sub["username"],sub["title"],sub["internal_id"],int(time.time()),))
                cursore.execute("SELECT social_id FROM socials WHERE social = ? AND internal_id = ?;", (sub["social"],sub["internal_id"],))
                sub["social_id"] = cursore.fetchone()[0]
                cursore.execute("INSERT INTO registrations (user_id, social_id) VALUES (?, ?);", (userId, sub["social_id"],))
                connettiDatabase.commit()
                cursore.close()
                connettiDatabase.close()
                sub["subStatus"] = "CreatedSocialAccAndSubscribed"
                return sub
            else:
                cursore.close()
                connettiDatabase.close()
                sub["subStatus"] = "notInDatabase"
                return sub
        #Se si è arrivati a questo punto significa che è presente nel database, ora bisogna controllare se l'utente è iscritto
        cursore.execute("SELECT 1 FROM registrations WHERE user_id = ? AND social_id = ?;", (userId, sub["social_id"],))
        try:
            #Utente già iscritto
            if cursore.fetchone()[0]:
                sub["subStatus"] = "AlreadySubscribed"
        except:
            #Utente non ancora iscritto, ora procedo ad iscriverlo
            sub["subStatus"] = "JustSubscribed"
            cursore.execute("INSERT INTO registrations (user_id, social_id) VALUES (?, ?);", (userId, sub["social_id"],))
            connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()
        return sub


    def get_social_id(database, internal_id):
        tabella = "socials"
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT social_id FROM socials WHERE social = ? AND internal_id = ?;", (internal_id["social"],internal_id["internal_id"],))
        try:
            exists = cursore.fetchone()[0]
        except:
            exists = None
        cursore.close()
        connettiDatabase.close()
        return exists


    def create_socialAcc_and_subscribeUser(database, userId, internal_id, tempo):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("INSERT INTO socials (social, username, internal_id, retreive_time) VALUES (?, ?, ?, ?);", (internal_id["social"],internal_id["username"],internal_id["internal_id"],tempo,))
        cursore.execute("SELECT social_id FROM socials WHERE social = ? AND internal_id = ?;", (internal_id["social"],internal_id["internal_id"],))
        social_id = cursore.fetchone()[0]
        cursore.execute("INSERT INTO registrations (user_id, social_id) VALUES (?, ?);", (userId, social_id,))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    #Vedere se l'if else dentro il try catch sia veramente necessario
    def check_if_subscribed(database, userId, social_id):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT 1 FROM registrations WHERE user_id = ? AND social_id = ?;", (userId, social_id,))
        try:
            if cursore.fetchone()[0]:
                exists = True
            else:
                exists = False
        except:
            exists = False
        cursore.close()
        connettiDatabase.close()
        return exists


    def subscribe(database, userId, social_id):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("INSERT INTO registrations (user_id, social_id) VALUES (?, ?);", (userId,social_id,))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def unsubscribe_user(database, userId):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("PRAGMA foreign_keys = ON") #Mi tocca specificarglielo di continuo, altrimenti sqlite fa la cacca e mi esegue la query con il supporto alle chiavi esterne spento >:(
        cursore.execute("DELETE FROM users WHERE user_id = ?;", (userId,))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def create_dict_of_userIds_and_socials(database):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT registrations.user_id, socials.social, socials.internal_id, socials.username, socials.title, socials.retreive_time, socials.status FROM registrations, socials WHERE registrations.social_id = socials.social_id;")
        socials_accounts_dict = {"social_accounts":{}, "subscriptions":{}}
        for row in cursore:
            if row[1] not in socials_accounts_dict["subscriptions"]:
                socials_accounts_dict["subscriptions"][row[1]] = {}
                socials_accounts_dict["social_accounts"][row[1]] = []
            if row[2] not in socials_accounts_dict["subscriptions"][row[1]]:
                socials_accounts_dict["subscriptions"][row[1]][row[2]] = []
                account_temp = {}
                account_temp["internal_id"] = str(row[2])
                account_temp["username"] = str(row[3])
                account_temp["title"] = str(row[4])
                account_temp["retreive_time"] = str(row[5])
                account_temp["status"] = str(row[6])
                socials_accounts_dict["social_accounts"][row[1]].append(account_temp)
            socials_accounts_dict["subscriptions"][row[1]][row[2]].append(str(row[0]))
        cursore.close()
        connettiDatabase.close()
        #print(socials_accounts_dict)
        return socials_accounts_dict


    def create_dict_of_a_user_subscriptions(database, userId):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT socials.social, socials.title, socials.internal_id FROM registrations, socials WHERE  registrations.user_id = ? AND registrations.social_id = socials.social_id;", (userId,))
        socials_accounts_dict = {}
        for row in cursore:
            if row[0] not in socials_accounts_dict: #Se la chiave <nomeSocial> non è stata ancora inizializzata allora lo faccio ora
                socials_accounts_dict[row[0]] = []
            socials_accounts_dict[row[0]].append({"title":str(row[1]), "internal_id":str(row[2])})
        cursore.close()
        connettiDatabase.close()
        #print(socials_accounts_dict)
        return socials_accounts_dict


    def clean_dead_subscriptions(database):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("DELETE FROM socials WHERE social_id IN (SELECT social_id FROM socials WHERE social_id NOT IN (SELECT social_id FROM registrations));")
        print("Account senza iscrizioni rimossi: " + str(cursore.rowcount))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def check_number_subscribtions(database, userId):
        tabella = "registrations"
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("SELECT users.user_id, (SELECT count(user_id) AS registrations FROM registrations WHERE user_id = ?) AS actual_registrations, users.max_registrations FROM users where users.user_id = ?;", (userId, userId,))
        value = [column for column in cursore.fetchone()]
        value = {"user_id": value[0], "actual_registrations": value[1], "max_registrations": value[2]}
        cursore.close()
        connettiDatabase.close()
        return value


    def unfollow_social_account(database, user_id, social, internal_id):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("DELETE FROM registrations WHERE registrations.user_id = ? AND registrations.social_id = (SELECT socials.social_id FROM socials WHERE socials.social = ? AND socials.internal_id = ?);", (user_id,social,internal_id,))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    #Funzione orfana, ma può tornare utile in futuro
    def delete_social_account(database, social, internal_id):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("PRAGMA foreign_keys = ON")
        cursore.execute("DELETE FROM registrations WHERE registrations.social_id = (SELECT socials.social_id FROM socials WHERE socials.social = ? AND socials.internal_id = ?);", (social,internal_id,))
        cursore.execute("DELETE FROM socials WHERE socials.social = ? AND socials.internal_id = ?;", (social,internal_id,))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def process_messages_queries(database, messages_queries):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("PRAGMA foreign_keys = ON")
        for query in messages_queries["update"]:
            if query["type"] == "retreive_time":
                cursore.execute("UPDATE socials SET retreive_time = ? WHERE social = ? AND internal_id = ?;", (query["retreive_time"],query["social"],query["internal_id"],))
            elif query["type"] == "status":
                cursore.execute("UPDATE socials SET status = ? WHERE social = ? AND internal_id = ?;", (query["status"],query["social"],query["internal_id"],))
        for query in messages_queries["delete"]:
            if query["type"] == "socialAccount":
                cursore.execute("DELETE FROM registrations WHERE registrations.social_id = (SELECT socials.social_id FROM socials WHERE socials.social = ? AND socials.internal_id = ?);", (query["social"],query["internal_id"],))
                cursore.execute("DELETE FROM socials WHERE socials.social = ? AND socials.internal_id = ?;", (query["social"],query["internal_id"],))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()


    def subscribe_user(database, userId, username, chat_id, notifications, max_registrations):
        connettiDatabase = sqlite3.connect(database)
        cursore = connettiDatabase.cursor()
        cursore.execute("INSERT INTO users (user_id, username, chat_id, notifications, max_registrations, subscription_time) VALUES (?, ?, ?, ?, ?, ?);", (userId,username,chat_id,notifications, max_registrations, int(time.time())))
        connettiDatabase.commit()
        cursore.close()
        connettiDatabase.close()



