import time
import requests
from sender_receiver import Get_data

class Instagram:
    def instagram(social_accounts):

        #print(social_accounts)

        messages = []
        queries = {}
        queries["update"] = []
        queries["delete"] = []


        for value in social_accounts:

            userId = value["internal_id"]
            user = value["username"]
            title = value["title"]
            status = value["status"]


            print("Getting JSON from Instagram of "+user+"...")
            f = Get_data.get_json_from_url("https://www.instagram.com/"+user+"/", "instagram")


            if f["entry_data"]: #Controlla se l'account è ancora esistente o meno
                if f["entry_data"]["ProfilePage"][0]["graphql"]["user"]["is_private"]: #Controlla se il profilo è privato o meno
                    if status != "private": #Se prima non era privato allora cambia il suo status, altrimenti non fare nulla
                        print("Il profilo "+user+" è diventato privato")
                        queries["update"].append({'type':'status', 'status':'private', 'social':'instagram', 'internal_id': ''+userId+''})
                        messages.append({"type": "now_private" ,"social": "instagram", "internal_id": userId, "username": user, "post_url": "https://www.instagram.com/"+user+"/", "post_date": int(time.time())})
                else:
                    if status == "private": #Se prima era privato allora cambia il suo status, altrimenti non fare nulla
                        print("Il profilo "+user+" da privato è ritornato pubblico")
                        queries["update"].append({'type':'status', 'status':'public', 'social':'instagram', 'internal_id': ''+userId+''})

                    lastPostDate = int(value["retreive_time"])#-172800

                    #print("Got "+str(lastPostDate))


                    lastPostDate_Temp = lastPostDate

                    username = f["entry_data"]["ProfilePage"][0]["graphql"]["user"]["username"]

                    for post in f["entry_data"]["ProfilePage"][0]["graphql"]["user"]["edge_owner_to_timeline_media"]["edges"]:
                        taken_at_timestamp = int(post["node"]["taken_at_timestamp"])
                        if taken_at_timestamp > lastPostDate:
                            shortcode = post["node"]["shortcode"]
                            source_url = post["node"]["display_url"]
                            try:
                                media_description = post["node"]["edge_media_to_caption"]["edges"][0]["node"]["text"]
                                messages.append({"type": "new_post" ,"social": "instagram", "internal_id": userId, "username": username, "title": title, "post_title": None, "post_description": media_description, "post_url": "https://www.instagram.com/p/"+shortcode+"/", "media_source":source_url, "post_date": taken_at_timestamp})
                            except (KeyError, IndexError):
                                messages.append({"type": "new_post" ,"social": "instagram", "internal_id": userId, "username": username, "title": title, "post_title": None, "post_description": None, "post_url": "https://www.instagram.com/p/"+shortcode+"/", "media_source":source_url, "post_date": taken_at_timestamp})
                            if int(taken_at_timestamp) > lastPostDate_Temp:
                                lastPostDate_Temp = int(taken_at_timestamp)

                    lastPostDate = lastPostDate_Temp



                    queries["update"].append({'type':'retreive_time', 'social':'instagram', 'internal_id': ''+userId+'', 'retreive_time': ''+str(lastPostDate)+''})

            else:
                #Se l'account non esiste più allora lo cancello
                print("Il profilo "+user+" non esiste più, ora lo cancello.")
                queries["delete"].append({'type':'socialAccount', 'social':'instagram', 'internal_id': ''+userId+''})
                messages.append({"type": "deleted_account" ,"social": "instagram", "internal_id": userId, "username": user, "title": title, "post_url": "https://www.instagram.com/"+user+"/", "post_date": int(time.time())})


        #Messaggi ordinati cronologicamente
        messages.reverse()
        
        return {'messages': messages, 'queries': queries}
