from database import Database
import re
import time
import urllib.request
import requests
import json

from sender_receiver import Get_data


def checkUrlValidity(sub):
    if sub["link"]:
        sub["data"] = {}
        #Instagram
        tmp = re.search("(?:https:\/\/www\.|www\.)?instagram\.com\/p\/([A-Za-z0-9-_]{1,30})", sub["link"])
        if tmp:
            sub["social"] = "instagram"
            sub["link"] = tmp.group(0)
            sub["data"]["p"] = tmp.group(1)
            return sub
        tmp = re.search("(?:https:\/\/www\.|www\.)?instagram\.com\/([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)", sub["link"])
        if tmp:
            sub["social"] = "instagram"
            sub["link"] = tmp.group(0)
            sub["username"] = tmp.group(1)
            return sub
        #Youtube
        tmp = re.search("(?:https:\/\/)?(?:www\.|m\.)?youtube\.com\/channel\/([A-Za-z0-9-_]{1,48})", sub["link"])
        if tmp:
            sub["social"] = "youtube"
            sub["link"] = tmp.group(0)
            sub["internal_id"] = tmp.group(1)
            return sub
        tmp = re.search("(?:https:\/\/)?(?:www\.|m\.)?youtube\.com\/user\/([A-Za-z0-9-_]{1,48})", sub["link"])
        if tmp:
            sub["social"] = "youtube"
            sub["link"] = tmp.group(0)
            sub["username"] = tmp.group(1)
            return sub
        tmp = re.search("(?:https:\/\/)?(?:www\.|m\.)?youtube\.com\/watch\?v=([A-Za-z0-9-_]{1,48})", sub["link"])
        if tmp:
            sub["social"] = "youtube"
            sub["link"] = tmp.group(0)
            sub["data"]["v"] = tmp.group(1)
            return sub
        tmp = re.search("(?:https:\/\/)?youtu\.be\/([A-Za-z0-9-_]{1,48})", sub["link"])
        if tmp:
            sub["social"] = "youtube"
            sub["link"] = tmp.group(0)
            sub["data"]["v"] = tmp.group(1)
            return sub
        #Flickr
        tmp = re.search("(?:http[s]?:\/\/)?(?:www\.|m\.)?flickr\.com\/(?:photos\/|people\/)?([A-Za-z0-9-_]{1,48})", sub["link"])
        if tmp:
            sub["social"] = "flickr"
            sub["link"] = tmp.group(0)
            sub["username"] = tmp.group(1)
            return sub

    return sub


def extractDataOnline(sub, apikeys):
    if sub["social"] == "instagram":
        if sub["username"]:
            f = Get_data.get_json_from_url("https://www.instagram.com/"+sub["username"]+"/", "instagram")
            if f["entry_data"]: #se non è vuoto (quindi esiste l'account social)
                sub["internal_id"] = f["entry_data"]["ProfilePage"][0]["graphql"]["user"]["id"]
                sub["title"] = sub["username"]
                sub["subStatus"] = "subscribable"
                if f["entry_data"]["ProfilePage"][0]["graphql"]["user"]["is_private"]:
                    sub["status"] = "private"
                else:
                    sub["status"] = "public"
            else:
                sub["subStatus"] = "NotExists"
                sub["status"] = "unknown"
        elif "p" in sub["data"]:
            f = Get_data.get_json_from_url("https://www.instagram.com/p/"+sub["data"]["p"]+"/", "instagram")
            try: #se genera l'eccezione vuol dire che il link di instagram per il post (video od immagine che sia) non è valido od è privato
                sub["username"] = f["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]["username"]
                sub["title"] = f["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]["username"]
                sub["internal_id"] = f["entry_data"]["PostPage"][0]["graphql"]["shortcode_media"]["owner"]["id"]
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExistsOrPrivate"
                sub["status"] = "unknown"
        else:
            sub["subStatus"] = "noSpecificMethodToExtractData"
            sub["status"] = "unknown"
    elif sub["social"] == "youtube":
        if sub["internal_id"]:
            f = Get_data.get_json_from_url("https://www.googleapis.com/youtube/v3/channels?id="+sub["internal_id"]+"&part=snippet%2CcontentDetails%2Cstatistics&key="+apikeys["youtubev3"], "youtube")
            try:
                sub["username"] = f["items"][0]["id"]
                sub["title"] = f["items"][0]["snippet"]["title"]
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExists"
                sub["status"] = "unknown"
        elif sub["username"]:
            f = Get_data.get_json_from_url("https://www.googleapis.com/youtube/v3/channels/?forUsername="+sub["username"]+"&part=snippet%2CcontentDetails%2Cstatistics&key="+apikeys["youtubev3"], "youtube")
            try:
                sub["internal_id"] = f["items"][0]["id"]
                sub["title"] = f["items"][0]["snippet"]["title"]
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExists"
                sub["status"] = "unknown"
        elif "v" in sub["data"]:
            f = Get_data.get_json_from_url("https://www.googleapis.com/youtube/v3/videos?id="+sub["data"]["v"]+"&part=snippet%2CcontentDetails%2Cstatistics&key="+apikeys["youtubev3"], "youtube")
            try:
                sub["internal_id"] = f["items"][0]["snippet"]["channelId"]
                sub["username"] = f["items"][0]["snippet"]["channelId"]
                sub["title"] = f["items"][0]["snippet"]["channelTitle"]
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExistsOrPrivate"
                sub["status"] = "unknown"
        else:
            sub["subStatus"] = "noSpecificMethodToExtractData"
            sub["status"] = "unknown"
    elif sub["social"] == "flickr":
        if sub["internal_id"]:
            f = Get_data.get_json_from_url("http://api.flickr.com/services/feeds/photos_public.gne?id="+sub["internal_id"]+"&format=json", "flickr-internal_id")
            try:
                tmp = re.search("photos\/([a-zA-Z0-9]{1,32})", f["link"])
                sub["username"] = tmp.group(1)
                sub["title"] = tmp.group(1)
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExists"
                sub["status"] = "unknown"
        elif sub["username"]:
            f = Get_data.get_json_from_url("https://www.flickr.com/photos/"+sub["username"]+"/", "flickr-username")
            try:
                sub["internal_id"] = f["internal_id"]
                sub["title"] = sub["username"]
                sub["subStatus"] = "subscribable"
                sub["status"] = "public"
            except:
                sub["subStatus"] = "NotExists"
                sub["status"] = "unknown"
        else:
            sub["subStatus"] = "noSpecificMethodToExtractData"
            sub["status"] = "unknown"
    else:
        #Se entra qua significa che la query (link o comando /sub) è stata accettata ma non c'è un metodo
        #per estrarre i dati, ergo, qualcuno si è dimenticato di aggiungere questo metodo, quindi verrà
        #considerato convenzionalmente come "profilo non esistente".
        sub["subStatus"] = "noMethodToExtractData"
        sub["status"] = "unknown"
    return sub


#This is in BETA, needs more testing!
#{"social":"", "username":"", "internal_id":"" , "social_id":"", "link":"", "data":{}}
def iscrizione(sub, userId, database, apikeys):
    #Spostare fuori quessto dizionario
    social_abilited = {"instagram": "instagram", "ig": "instagram", "youtube":"youtube", "yt":"youtube", "flickr":"flickr", "fr":"flickr"}

    #Check if there's enough data
    if not ((sub["social"] and sub["username"]) or sub["link"]):
        return None

    #Check user's number of subscriptions
    user_subs = Database.check_number_subscribtions(database, userId)
    if user_subs["actual_registrations"] >= user_subs["max_registrations"]:
        return "Max subs limit reached"

    #Extracts from string URL all data possible
    sub = checkUrlValidity(sub)
    if not sub["social"]:
        return None
    
    #Check if social is abilited
    if sub["social"] in social_abilited:
        sub["social"] = social_abilited[sub["social"]] #Uniforma tutti gli alias dei social ad un unico nome
    else:
        return "Social not abilited or mistyped"

    #Extract partial data locally on supported socials
    sub["subStatus"] = "social_id_not_present"
    social_user_search_supported = {"instagram": None, "youtube": None, "flickr": None}
    if (sub["username"] or sub["internal_id"]) and (sub["social"] in social_user_search_supported):
        sub = Database.get_first_social_id_from_internal_user_social_and_if_present_subscribe(database, userId, sub, False)

    #È uguale a: sub["subStatus"] == "notInDatabase" or sub["subStatus"] == "internalId_or_username_not_present"
    #Check if from your local data (db and url string) you've been able to extract the social_id
    #Note1: If you have social_id means that it's also already in the database, ergo: it has already been processed
    #Note2: Significa che a questo punto hai fatto del tuo meglio per estrarre i dati localmente, e ti trovi di fronte a tre casi:
    # 1) È stato inserito un link da cui è impossibile estrarre la username o l'id interno (il social lo si riesce ad estrarre sempre)
    # 2) I dati estratti non hanno trovato riscontro nel database locale, quindi è necessario ricercare online
    # 3) I dati estratti hanno trovato riscontro nel database e l'utente è risultato O già iscritto O non ancora iscritto, in quest'ultimo
    #    caso l'untente viene automaticamente iscritto subito e questo if viene saltato
    if not sub["social_id"]:
        #Extract data online
        sub = extractDataOnline(sub, apikeys)
        #If exists, check if already in the database
        #If in the database subscribe, else add and subscribe
        if sub["subStatus"] == "subscribable":
            sub = Database.get_first_social_id_from_internal_user_social_and_if_present_subscribe(database, userId, sub, True)

    if sub["subStatus"] == "JustSubscribed" or sub["subStatus"] == "CreatedSocialAccAndSubscribed":
        if sub["status"] == "public":
            return "Social: "+sub["social"]+"\nUser: "+sub["title"]+"\nYou've been successfully subscribed!\nFrom now on, you'll start to receive feeds from this account!"
        elif sub["status"] == "private":
            return "Social: "+sub["social"]+"\nUser: "+sub["title"]+"\nYou've been subscribed to a social account that is private!\nYou'll not receive feeds until it switches to public!"
        else:
            return  "Social: "+str(sub["social"])+"\nUser: "+str(sub["title"])+"\nMmmh, something went really wrong, the status is unknown :/\nYou should get in touch with the admin!"
    elif sub["subStatus"] == "AlreadySubscribed":
        return "Social: "+sub["social"]+"\nUser: "+sub["title"]+"\nYou're already subscribed to this account!"
    elif sub["subStatus"] == "NotExists":
        return "Social: "+sub["social"]+"\nThis account doesn't exists!"
    elif sub["subStatus"] == "NotExistsOrPrivate":
        return "Social: "+sub["social"]+"\nThis account doesn't exists or is private!"
    elif sub["subStatus"] == "noSpecificMethodToExtractData" or sub["subStatus"] == "noMethodToExtractData":
        return "Social: "+str(sub["social"])+"\nMmmh, this shouldn't happen, no method (or specific method) to extract data."
    else:
        return "Social: "+str(sub["social"])+"\nI don't know what happened! O_o\""


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


def helpPack():
    return "📖Help\n\nYou can follow up to <i>10 social accounts</i>.\n\
Socials currently supported:\n\
  • <i>Instagram</i>\n\
  • <i>Youtube</i>\n\
  • <i>Flickr</i>\n\
You can follow only <b>public</b> accounts.\n\
\n\
<b>Receive Feeds:</b>\n\
  • Paste here the link of the homepage or of a shared post of your target account.\n\
  • /sub <i>social</i> <i>username</i>\n\
  • /sub <i>link</i>\n\
To unfollow and see the list of the accounts you're following use the inline buttons below.\n\
Use /help or /list commands to invoke help or list menu.\n\
/stop to stop and unsubscribe from the bot.\n\
That's all. :)"


def todoPack():
    return "TODO\n\
  • Nell'elenco delle iscrizioni indicare se l'account è privato.\n\
  • Aggiungere l'iscrizione ad un account social inserendo solo social ed id_interno.\n\
  • Finire di sistemare la parte dei pulsanti che alcuni sono ancora dummy.\n\
  • Aggiungere il pulsante REFRESH.\n\
  • Aggiungere il supporto nella parte di scheduling dei messaggi alle funzioni che non prevedono il chat_id.\n\
  • Aggiungere tool di base per gli Admin.\n\
  • Aggiungere statistiche di base sia per gli admin che per gli utenti.\n\
  • Rendere il bot multilingua.\n\
  • Rilevare la lingua del client.\n\
  • Riscrivere el istruzioni nell'UI e scrivere un minimo di documentazione.\n\
  • Aggiungere i controlli quando vengono caricati i parametri dal file di config.\n\
  • Aggiornare in caso l'account social cambi username.\n\
  • Questo controllo in teoria lo si dovrebbe fare sia durante l'aggiunta del profilo che durante il retreive dei dati.\n\
  • Rilevare quando l'utente blocca il bot e disiscriverlo.\n\
  • Aggiungere un thread che gestisca in maniera separata le iscrizioni ai social. Il tempo per contattare i server dei social è troppo lungo.\n\
  • Aggiungere il supporto a:\n\
     • Artstation\n\
     • Twitch\n\
     • Deviantart\n\
     • Usplash\n\
     • Pixiv\n\
     • Tumblr (controllare)\n\
     • Twitter\n\
     • Tntvillage (?)\n\
     • Google Plus"


def listPack(database, userId):
    user_subscriptions_dict = Database.create_dict_of_a_user_subscriptions(database, userId)
    pack = "👥Follow List\n\nYou are following:\n"
    for key, value in user_subscriptions_dict.items():
        pack = pack + str(key) + "\n"
        for username in value:
            pack = pack + "  • " + str(username["title"]) + "\n"
    return pack


def list_removePack(database, userId):
    user_subscriptions_dict = Database.create_dict_of_a_user_subscriptions(database, userId)
    pack = "♻️Remove\n\nYou are following:\n"
    dictionary_pack = {}
    temporary_buttons_list = []
    i = 1
    k = 0
    tmp = []
    for key, value in user_subscriptions_dict.items():
        pack = pack + str(key) + "\n"
        for username in value:
            if k%5 == 0:
                if k > 0:
                    temporary_buttons_list.append(tmp)
                tmp = []
            tmp.append({"text":str(i),"callback_data":"remove "+str(key)+" "+username["internal_id"]})
            pack = pack + "  " + alignCols(replace_all(str(i), {"0":"⓪", "1":"①", "2":"②", "3":"③", "4":"④", "5":"⑤", "6":"⑥", "7":"⑦", "8":"⑧", "9":"⑨"}), 2, 1, ".") + str(username["title"]) + "\n"
            k = k + 1
            i = i + 1
    if len(tmp) > 0:
        temporary_buttons_list.append(tmp)
    temporary_buttons_list.append([{"text":"«","callback_data":"remove_mode back"},{"text":"✏️","callback_data":"add_mode"},{"text":"📊","callback_data":"list_mode"},{"text":"📖","callback_data":"help_mode"},{"text":"»","callback_data":"remove_mode forward"}])
    dictionary_pack["inline_keyboard"] = temporary_buttons_list
    dictionary_pack["text"] = pack
    return dictionary_pack


def alignCols(word, lengthmax, distance, puntini_sospensione):
    lunghezza_puntini = len(puntini_sospensione)
    word = (word[:lengthmax - lunghezza_puntini] + puntini_sospensione) if len(word) > lengthmax else word
    spaces = ""
    for count in range(0,lengthmax + distance - len(word)):
        spaces = spaces + " "
    return word + spaces



class Process_input:


    def process_input(updates, database, apikeys):
        messages = []
        for update in updates["result"]:
            if 'message' in update:
                messX = "message"
            elif 'edited_message' in update:
                messX = "edited_message"
            elif 'callback_query' in update:
                messX = "callback_query"
            else:
                messX = "WTF" #trovare un modo per gestire questo caso
            userId = update[messX]["from"]["id"]
            if "username" in update[messX]["from"]:
                username = update[messX]["from"]["username"]
            else:
                username = ""
            if "chat" in update[messX]: #di solito arrivano quando ci sono i messaggi "normali"
                chat_id = update[messX]["chat"]["id"]
            elif "message" in update[messX]: #di solito arrivando quando ci sono le callback_query
                chat_id = update[messX]["message"]["chat"]["id"]
            else:
                chat_id = update[messX]["WTF"] #trovare un modo per gestire questo caso
            if Database.checkUtente(database, "users", userId, "userIdExists"):
                if 'text' in update[messX]:
                    text = update[messX]["text"]
                    if text[:1] == "/":
                        if text == "/stop":
                            Database.unsubscribe_user(database, userId)
                            if Database.checkUtente(database, "users", userId, "userIdExists"):
                                messages.append({"type": "sendMessage", "text":"Something bad happened, you're STILL registered!\nTry again later.", "chat_id":chat_id, "markdown":False})
                            else:
                                messages.append({"type": "sendMessage", "text":"You're no longer subscribed!\nWe already <i>miss</i> you, please come back soon! 😢\nTip: In order to re-joyn type \start *wink* *wink*", "chat_id":chat_id, "markdown":"HTML"})
                        elif text == "/follow":
                            messages.append({"type": "sendMessage", "text":"Start follow someone.", "chat_id":chat_id, "markdown":False})
                        elif text == "/unfollow":
                            messages.append({"type": "sendMessage", "text":"Stop follow someone.", "chat_id":chat_id, "markdown":False})
                        elif text == "/list":
                            messages.append({"type": "sendMessage", "text":listPack(database, userId), "chat_id":chat_id, "reply_markup":{"inline_keyboard":[[{"text":"🗑","callback_data":"remove_mode"},{"text":"✏️","callback_data":"add_mode"}],[{"text":"🔕","callback_data":"notifications_mode_off"},{"text":"📖","callback_data":"help_mode"}]]}})
                        elif text == "/help":
                            messages.append({"type": "sendMessage", "text":helpPack(), "chat_id":chat_id, "markdown":"HTML", "reply_markup":{"inline_keyboard":[[{"text":"🗑","callback_data":"remove_mode"},{"text":"✏️","callback_data":"add_mode"}],[{"text":"🔕","callback_data":"notifications_mode_off"},{"text":"📊","callback_data":"list_mode"}]]}})
                        elif text == "/todo":
                            messages.append({"type": "sendMessage", "text":"<code>"+todoPack()+"</code>", "chat_id":chat_id, "markdown":"HTML", "reply_markup":{"inline_keyboard":[[{"text":"🗑","callback_data":"remove_mode"},{"text":"✏️","callback_data":"add_mode"}],[{"text":"🔕","callback_data":"notifications_mode_off"},{"text":"📊","callback_data":"list_mode"}]]}})
                        elif text[:5] == "/sub ":
                            match = re.search("^([a-zA-Z]{1,16}) (\S{1,128})$", text[5:])
                            if match:
                                msg_subs = iscrizione({"social":match.group(1), "username":match.group(2), "internal_id":None , "social_id":None, "link":None, "data":{}}, userId, database, apikeys)
                            else:
                               match = re.search("^(http[s]?:\/\/)?([a-zA-Z0-9-_]{2,256}\.\S*)$", text[5:])
                               if match:
                                   msg_subs = iscrizione({"social":None, "username":None, "internal_id":None , "social_id":None, "link":match.group(2), "data":{}}, userId, database, apikeys)
                               else:
                                   msg_subs = None
                            if not msg_subs:
                                msg_subs = "/sub command badly compiled"
                            messages.append({"type": "sendMessage", "text":msg_subs, "chat_id":chat_id, "markdown":False})

                        elif text[:9] == "/promote " or text[:8] == "/demote " or text[:6] == "/kick ":
                            if text[:9] == "/promote ":
                                tmp = 9
                                action = "promote"
                            elif text[:8] == "/demote ":
                                tmp = 8
                                action = "demote"
                            else:
                                tmp = 6
                                action = "kick"
                            msg_promo = {}
                            match = re.search("^([0-9]{1,16})$", text[tmp:])
                            if match:
                                msg_promo = Database.adminActions(int(match.group(1)), userId, action, database)
                                if msg_promo["success"]:
                                    msg_promo["message"] = "Success!\nReason: "+msg_promo["reason"]
                                else:
                                    if msg_promo["reason"] == "noPower":
                                        msg_promo["message"] = "Failure!\nReason: "+msg_promo["reason"]+"\nYou're not the Queen!!\nSpit on the false Queen!"
                                    else:
                                        msg_promo["message"] = "Failure!\nReason: "+msg_promo["reason"]
                            else:
                                msg_promo["message"] = "/promote command badly compiled"
                            messages.append({"type": "sendMessage", "text":msg_promo["message"], "chat_id":chat_id, "markdown":False})

                        else:
                            messages.append({"type": "sendMessage", "text":"Unrecognized command", "chat_id":chat_id, "markdown":False})
                    else:
                        hotlink_flag = True #TODO: Move this flag into the config file
                        if hotlink_flag:
                            match = re.search("(http[s]?:\/\/)?([a-zA-Z0-9-_]{2,256}\.[\S]*)", text)
                            if match:
                               msg_subs = iscrizione({"social":None, "username":None, "internal_id":None , "social_id":None, "link":match.group(2), "data":{}}, userId, database, apikeys)
                            else:
                               msg_subs = None
                            if msg_subs: #If contains something send, else ignore
                                messages.append({"type": "sendMessage", "text":msg_subs, "chat_id":chat_id, "markdown":False})
                #"chat_id":chat_id non è prevista dalle API di Telegram per answerCallbackQuery, serve solo alla funzione Sender.send_messages(coda, condizione)
                #che si basa sul parametro "chat_id" per schedualare i messaggi da inviare
                #TODO: Togliere i chat_id inutili non appena è stato risolto il problema nel send_messages perché questa è una soluzione triste (ma non troppo)
                #NOTA: Mi sa che questa "soluzione triste" rimarrà così per sempre, od almeno finchè Telegram non rilascerà specifiche più dettagliate.
                elif 'callback_query' in update:
                    callback_query_id = update[messX]["id"]
                    message_id = update[messX]["message"]["message_id"]
                    callback_data = update[messX]["data"]
                    if callback_data == "help_mode":
                        messages.append({"type": "answerCallbackQuery", "callback_query_id":callback_query_id, "text":"Help", "show_alert":False, "chat_id":chat_id})
                        messages.append({"type": "editMessageText", "message_id":message_id, "text":helpPack(), "chat_id":chat_id, "markdown":"HTML", "reply_markup":{"inline_keyboard":[[{"text":"🗑","callback_data":"remove_mode"},{"text":"✏️","callback_data":"add_mode"}],[{"text":"🔕","callback_data":"notifications_mode_off"},{"text":"📊","callback_data":"list_mode"}]]}})
                    elif callback_data == "list_mode":
                        messages.append({"type": "answerCallbackQuery", "callback_query_id":callback_query_id, "text":"Following list", "show_alert":False, "chat_id":chat_id})
                        messages.append({"type": "editMessageText", "message_id":message_id, "text":listPack(database, userId), "chat_id":chat_id, "reply_markup":{"inline_keyboard":[[{"text":"🗑","callback_data":"remove_mode"},{"text":"✏️","callback_data":"add_mode"}],[{"text":"🔕","callback_data":"notifications_mode_off"},{"text":"📖","callback_data":"help_mode"}]]}})
                    elif callback_data == "remove_mode":
                        list_remove_dict = list_removePack(database, userId)
                        messages.append({"type": "answerCallbackQuery", "callback_query_id":callback_query_id, "text":"Be careful, you'll not receive confirmation alert upon removing!", "show_alert":True, "chat_id":chat_id})
                        messages.append({"type": "editMessageText", "message_id":message_id, "text":list_remove_dict["text"], "chat_id":chat_id, "reply_markup":{"inline_keyboard":list_remove_dict["inline_keyboard"]}})
                    elif re.findall("^remove (\S+) (\S+)", callback_data):
                        command_query = []
                        for command in re.findall("^remove (\S+) (\S+)", callback_data)[0]:
                            command_query.append(command)
                        Database.unfollow_social_account(database, userId, command_query[0], command_query[1])
                        list_remove_dict = list_removePack(database, userId)
                        messages.append({"type": "answerCallbackQuery", "callback_query_id":callback_query_id, "text":"Unfollowed", "show_alert":False, "chat_id":chat_id})
                        messages.append({"type": "editMessageText", "message_id":message_id, "text":list_remove_dict["text"], "chat_id":chat_id, "reply_markup":{"inline_keyboard":list_remove_dict["inline_keyboard"]}})

                    #TODO:Fare un'implementazione minima della callback_query
                else:
                    messages.append({"type": "sendMessage", "text":"[AUTHORIZED] You can send text only!", "chat_id":chat_id, "markdown":False})
            else:
                if 'text' in update[messX]:
                    text = update[messX]["text"]
                    if text == "/start":
                        Database.subscribe_user(database, userId, username, chat_id, 1, 10)
                        if Database.checkUtente(database, "users", userId, "userIdExists"):
                            messages.append({"type": "sendMessage", "text":"Congratulations, you're now registered!\nType /help to learn the commands available!", "chat_id":chat_id, "markdown":False})
                        else:
                            messages.append({"type": "sendMessage", "text":"Something bad happened, you're NOT registered!\nTry again later.", "chat_id":chat_id, "markdown":False})
                    else:
                        messages.append({"type": "sendMessage", "text":"You're not registered, type /start to subscribe!", "chat_id":chat_id, "markdown":False})

        return messages




