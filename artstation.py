import urllib.request
import json
import sqlite3
import time
import calendar
import datetime
import requests

from database import Database


def get_url(url):
    i = 0
    while True:
        try:
            return requests.get(url, timeout=(3, 300)).content.decode("utf8")
        except:
            print("Connection lost or some other darky error like 503 :(")
            time.sleep(2**i)
            print("Trying to connect...")
            if i < 6:
                i = i + 1



def get_lastPostDate(database, user):
    connettiDatabase = sqlite3.connect(database)
    cursore = connettiDatabase.cursor()
    query = ("SELECT retreive_time FROM socials WHERE social = 'artstation' AND internal_id = '"+user+"';")
    #print(query)
    cursore.execute(query)
    exists = []
    for row in cursore.fetchall():
        exists.append(row[0])
    cursore.close()
    connettiDatabase.close()
    return exists[0]




class Artstation:
    def artstation(users):

        database = "socialFeedgram.sqlite3"

        messages = []
        lastPostDates = []

        for user in users:

                
            print("Getting JSON from Artstation of "+user+"...")
            f = get_url("https://www.artstation.com/users/"+user+"/projects.json?page=1")
            f = json.loads(f)

            
            lastPostDate = int(get_lastPostDate(database, user))#-1728000

            lastPostDate_Temp = lastPostDate

            for post in f["data"]:
                a = post["published_at"]
                postDate = int(calendar.timegm(datetime.datetime.strptime(a[:-10], "%Y-%m-%dT%H:%M:%S").timetuple()))+int(a[-6:-3])*3600
                if postDate > lastPostDate:
                    messages.append({"social": "artstation", "username": user, "title": post["title"], "description": post["description"], "post_url": post["permalink"], "media_source": None, "post_date": postDate})
                    if postDate > lastPostDate_Temp:
                        lastPostDate_Temp = postDate

            lastPostDate = lastPostDate_Temp





            lastPostDates.append({'username': ''+user+'', 'lastPostDate': ''+str(lastPostDate)+''})

        for lastPostDate in lastPostDates:
            Database.eseguiQueryNoReturn(database, "UPDATE socials SET retreive_time = "+str(lastPostDate["lastPostDate"])+" WHERE social = 'artstation' AND internal_id = '"+lastPostDate["username"]+"';")


        #Messaggi ordinati cronologicamente
        messages.reverse()
        
        return messages




