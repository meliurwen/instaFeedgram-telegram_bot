import re
import os
import errno
import configparser



def controllaFile(fileConfig):

    #TUTTE le chiavi del dizionario quì sotto DEVONO essere scritte in minuscolo, altrimenti falliscono i controlli più avanti
    #(sì, lo so che potrei mettere un .lower(), ma anche no >:()
    default_config = {"BOT": {"databasefilepath": "socialFeedgram.sqlite3"}, "API": {"telegramkey": "", "youtubev3key": "", "googlepluskey": ""}}


    ##<inizio codice magico>
    ##Questo codice è in parte magia nera, non l'ho capito a fondo (thx Stackoverflow <3)
    print("Controllo se esiste già il file di configurazione \"" + fileConfig + "\"...")
    flags = os.O_CREAT | os.O_EXCL
    try:
        file_handle = os.open(fileConfig, flags)
        os.close(file_handle)
    except OSError as e:
        if e.errno == errno.EEXIST:  # Failed as the file already exists.
            print("Il file " + fileConfig + " esiste già.")
            pass
        else:  # Something unexpected went wrong so reraise the exception.
            print("Qualcosa è andato storto con la creazione del file " + fileConfig)
            raise
    else:
        print("Il file " + fileConfig + " NON esisteva, perciò è stato creato.")
        print("Imposto i valori di default...")
        with open(fileConfig, 'w') as fileConfigObj:
            configurazione = configparser.ConfigParser()
            for sezione in default_config:
                configurazione[sezione] = default_config[sezione]
            
            configurazione.write(fileConfigObj)
            print("Valori di default impostati con successo")
    ##</fine codice magico>


    configurazione = configparser.ConfigParser()
    try:
        configurazione.read(fileConfig)
    except configparser.DuplicateOptionError:
        print("Delle chiavi sono duplicate")
        return -3
    except configparser.DuplicateSectionError:
        print("Delle sezioni sono duplicate")
        return -3
    cofigurazione_dict = as_dict(configurazione) #Converto l'oggetto configurazione in un dizionario
    print("Controllo che ci siano esattamente tutte le sezioni...")
    if not (set(cofigurazione_dict.keys()) == set(default_config.keys())):
        print("NON ci sono ESATTAMENTE tutte le sezioni.")
        return -3
    print("Ci sono tutte le sezioni.")
    print("Controllo che in ogni sezione ci siano esattamente tutte le chiavi...")
    all_options_flag = True
    all_options_notEmpty_flag = True
    for key, value in default_config.items():
        if not (set(cofigurazione_dict[key].keys()) == set(default_config[key].keys())):
            print("Nella sezione "+str(key)+" NON ci sono ESATTAMENTE tutte le chiavi!")
            all_options_flag = False
        else:
            for key_section, value_section in cofigurazione_dict[key].items():
                if not str(value_section):
                    print("Nella sezione "+str(key)+" l'opzione "+str(key_section)+" è VUOTA!")
                    all_options_notEmpty_flag = False
    if not all_options_flag:
        print("NON in tutte le sezioni ci sono ESATTAMENTE tutte le chiavi!")
        return -3
    if not all_options_notEmpty_flag:
        print("NON in tutte le sezioni tutte le opzioni non sono VUOTE!") #Eheh, dovrei riformularla in maniera un po' più umana :)
        return -3
    print("Ci sono tutte le chiavi in ogni sezione.")


    print("Controllo che tutte le opzioni abbiano valori accettabli...")
    #In realtà bisognerebbe controllare se il path del database sia valido, ma vbb, lo farò un'altra volta
    #TODO: Implementare controllo per la validità del path del database
    print("Tutte le opzioni hanno valori accettabili.")

    print("Controllo conclusosi con successo.")
    return 1


def as_dict(self):
    d = dict(self._sections)
    for k in d:
        d[k] = dict(self._defaults, **d[k])
        d[k].pop('__name__', None)
    return d


class Config:

    def get_config(fileConfig):
        print("###################CARICAMENTO CONFIGURAZIONE#########################")

        ################CARICAMENTO CONFIGURAZIONE######################


        check = controllaFile(fileConfig)

        if check <= 0:
            return {"status": check, "config": {}}

        print("Caricamento impostazioni in corso...")
        ##Assegna un nome al file database ed estrae il token per il bot
        configurazione = configparser.ConfigParser()
        configurazione.read(fileConfig)
        config_dict = {"status" : check, "config": {}}
        config_dict["config"] = as_dict(configurazione)
        print("Caricamento impostazioni completato.")

        return config_dict

