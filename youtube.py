import time
import calendar
import datetime
import requests
from sender_receiver import Get_data

class Youtube:
    def youtube(social_accounts, api_key):

        messages = []
        queries = {}
        queries["update"] = []
        queries["delete"] = []

        for value in social_accounts:

            userId = value["internal_id"]
            user = value["username"]
            title = value["title"]
            status = value["status"]

            #https://www.googleapis.com/youtube/v3/playlistItems?order=date&part=snippet&playlistId=UU4V3oCikXeSqYQr0hBMARwg&maxResults=25&key=CHIAVE
            print("Getting JSON from Youtube of "+user+"...")
            f = Get_data.get_json_from_url("https://www.googleapis.com/youtube/v3/playlistItems?order=date&part=snippet&playlistId=UU"+userId[2:]+"&maxResults=25&key="+api_key, "youtube")

            request_status = True
            if "error" in f: #Controllo che non ci siano errori come account non esistente (o cancellato) o chiave non valida
                reason = f["error"]["errors"][0]["reason"]
                request_status = False
            elif "items" in f: #Controllo che il canale non sia vuoto
                if not f["items"]:
                    reason = "emptyChannel"
                    request_status = False

            if request_status: #Se l'account esiste e non è vuoto

                item_lastDate = int(value["retreive_time"])#-172800

                item_lastDate_Temp = item_lastDate
                for post in f["items"]:
                    chan_title = post["snippet"]["channelTitle"]
                    item_title = post["snippet"]["title"]
                    item_description = post["snippet"]["description"]
                    item_url = post["snippet"]["resourceId"]["videoId"]
                    youtubeDate = post["snippet"]["publishedAt"]
                    item_date = int(calendar.timegm(datetime.datetime.strptime(youtubeDate[:-5]+"Z", "%Y-%m-%dT%H:%M:%SZ").timetuple()))
                    if item_date > item_lastDate:
                        if "videoId" in post["snippet"]["resourceId"]:
                            messages.append({"type": "new_post", "social": "youtube", "internal_id": userId, "username": user, "title": chan_title, "post_title": item_title, "post_description": item_description, "post_url": "https://www.youtube.com/watch?v="+item_url, "media_source": None, "post_date": item_date})
                        else:
                            #Se entra nell'else inviare un alert all'utente (quando verrà implementato)
                            messages.append({"type": "new_post", "social": "youtube", "internal_id": userId, "username": user, "title": chan_title, "post_title": item_title, "post_description": "Errore: "+post["snippet"]["resourceId"], "post_url": "https://www.youtube.com", "media_source": None, "post_date": item_date})
                        if item_date > item_lastDate_Temp:
                            item_lastDate_Temp = item_date

                item_lastDate = item_lastDate_Temp

                queries["update"].append({'type':'retreive_time', 'social':'youtube', 'internal_id': userId, 'retreive_time': str(item_lastDate)})

            else:
                if reason == "playlistNotFound":
                    #Se l'account non esiste più allora lo cancello
                    print("Il profilo "+user+" non esiste più, ora lo cancello.")
                    queries["delete"].append({'type':'socialAccount', 'social':'youtube', 'internal_id': ''+userId+''})
                    messages.append({"type": "deleted_account" ,"social": "youtube", "internal_id": userId, "username": user, 'title': title, "post_url": "https://www.youtube.com/channel/"+userId, "post_date": int(time.time())})
                #TODO:Implementare un sistema che invii la notifica all'admin in caso di *keyInvalid* ed oltrepasso del rate limit
                elif reason == "keyInvalid":
                    pass #API key non valida
                elif reason == "emptyChannel":
                    pass
                else:
                    pass #Oltrepassato il rate limit od errore sconosciuto


        #Messaggi ordinati cronologicamente
        messages.reverse()
        
        return {'messages': messages, 'queries': queries}

