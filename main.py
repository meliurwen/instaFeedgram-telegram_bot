import time
import sys
import threading
import queue

from instagram import Instagram
from flickr import Flickr
from youtube import Youtube
from artstation import Artstation
from database import Database
from sender_receiver import Sender
from process_input import Process_input

from config import Config







fileConfig = "config.ini"

config_dict = Config.get_config(fileConfig)

if config_dict["status"] <= 0:
    print ("Arresto del programma...")
    sys.exit(0)

database = config_dict["config"]["BOT"]["databasefilepath"]
Sender.set_telegram_token(config_dict["config"]["API"]["telegramkey"])
apikeys = {'youtubev3': config_dict["config"]["API"]["youtubev3key"], 'googlePlus': config_dict["config"]["API"]["googlepluskey"]}




#Database.cattivo(database)


messages_socials =[]

print("######################CARICAMENTO DATABASE############################")
##Se il file NON esiste, creo il file database e le tabelle del database
##Se ESISTE controllo che siano presenti le tabelle
if Database.databaseExist(database):
    if not Database.tabelleExist(['admins', 'users', 'registrations', 'socials'], database):
        print ("Database presente ma mancano delle tabelle.")
        print ("Arresto del programma...")
        sys.exit(0)
    else:
        print("Tutte le tabelle sono presenti.")
else:
    print ("Creo il file SQLITE3 e le tabelle...")
    Database.eseguiQueryNoReturn(database, "CREATE TABLE `admins` (`user_id` INTEGER NOT NULL, `is_creator` INTEGER NOT NULL DEFAULT 0, FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE, PRIMARY KEY(`user_id`));")
    Database.eseguiQueryNoReturn(database, "CREATE TABLE `users` (`user_id` INTEGER NOT NULL, `username` TEXT, `chat_id` INTEGER NOT NULL, `notifications` INTEGER NOT NULL DEFAULT 1, `max_registrations` INTEGER NOT NULL DEFAULT 10, `subscription_time` INTEGER NOT NULL, PRIMARY KEY(`user_id`));")
    Database.eseguiQueryNoReturn(database, "CREATE TABLE `registrations` (`user_id` INTEGER NOT NULL, `social_id` INTEGER NOT NULL, PRIMARY KEY(`user_id`,`social_id`), FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE, FOREIGN KEY(`social_id`) REFERENCES `socials`(`social_id`) ON DELETE CASCADE ON UPDATE CASCADE);")
    Database.eseguiQueryNoReturn(database, "CREATE TABLE `socials` (`social_id`	INTEGER NOT NULL, `social` TEXT NOT NULL, `username` TEXT NOT NULL, `title` TEXT NOT NULL, `internal_id` TEXT NOT NULL, `retreive_time` INTEGER NOT NULL, `status` TEXT NOT NULL DEFAULT 'public', PRIMARY KEY(`social_id`));")
    print ("Database e tabelle create con successo")


print("####################FINE FASE DI CARICAMENTO##########################")




#Per la registrazione dell'admin, visto che lo si fa SOLO al primo avvio del bot, ho preferito implementare una routine a parte
print("Controllo se c'è già un creatore nel database...")
if not Database.creators_number(database):
    print("Non c'è un admin creatore, in attesa che il primo utente si registri inserendo nella chat un input qualsiasi...")
    firstMessage_flag = False
    last_update_id = None
    user_creator = {}
    while not firstMessage_flag:
        updates = Sender.get_updates(last_update_id)
        if len(updates["result"]) > 0:
            if 'message' in updates["result"][0]:
                messX = "message"
            else:
                messX = "edited_message"
            user_creator["user_id"] = str(updates["result"][0][messX]["from"]["id"])
            if "username" in updates["result"][0][messX]["from"]:
                user_creator["username"] = updates["result"][0][messX]["from"]["username"]
            else:
                username = ""
            user_creator["chat_id"] = updates["result"][0][messX]["chat"]["id"]

            Database.subscribe_and_promote_creator(database, user_creator)
            firstMessage_flag = True
            del last_update_id
        #time.sleep(10)
    print("Un utente si è registrato come admin creatore")
    Sender.send_message({'type': 'sendMessage', 'text': "<b>⚠️GOD POWERS⚠️</b>\nYou're the creator of this bot.", 'chat_id': ''+str(user_creator["chat_id"])+'', 'markdown': 'HTML'})
    del user_creator
else:
    print("Creatore già presente.")
    ##Almeno una tabella non è vuota
    ##TODO: Check integrity of the database





datas_social = []
still_run = True
subscriptions_dict = {}
condizione_compiler = threading.Condition()
condizione_sender = threading.Condition()
coda = queue.Queue()
coda_temp = queue.Queue()

def main():


    print("Lancio dei Thread")
    condizione = threading.Condition()
    thread1 = watchdog(1, "News_retreiver", "news_retreiver" , 600, condizione, still_run)
    thread2 = watchdog(1, "News_compiler", "news_compiler" , 0, condizione_compiler, still_run)
    thread3 = watchdog(1, "Telegram_user_interface", "telegram_user_interface" , 0, None, still_run) #T.U.I. aka Telegram User Interface (I hope to be the first to have invented this **original** name 8-) )
    thread4 = watchdog(1, "Sender", "sender", 0, condizione_sender, still_run)
    thread5 = watchdog(1, "Elaborazione_code", "elaborazione_code", 0, None, still_run)
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()
    thread5.start()


    while True:
        time.sleep(10)









class watchdog(threading.Thread):

    def news_retreiver_subthread(threadID, tSocial, subscriptions, coda_social, apikeys):
        if tSocial == "instagram":
            datas = Instagram.instagram(subscriptions)
        elif tSocial == "youtube":
            datas = Youtube.youtube(subscriptions, apikeys["youtubev3"])
        elif tSocial == "flickr":
            datas = Flickr.flickr(subscriptions)
        elif tSocial == "artstation":
            #datas = Artstation.artstation(social_accounts['artstation'])
            datas = []
        else:
            datas = []
        if datas:
            coda_social.put(datas)



    def __init__(self, threadID, name, mode, delay, condizione, still_run):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.mode = mode
        self.delay = delay
        self.condizione = condizione
        self.still_run = still_run
    def run(self):
        global datas_social
        global coda
        global coda_temp
        global subscriptions_dict


        if self.mode == "telegram_user_interface":
            last_update_id = None
            while still_run:
                updates = Sender.get_updates(last_update_id)
                if len(updates["result"]) > 0:
                    last_update_id = Sender.get_last_update_id(updates) + 1
                    coda_temp.put(Process_input.process_input(updates, database, apikeys))




        if self.mode == "elaborazione_code":
            delivering_list = []
            message_list = []
            #TODO: dare un'occhiata alla logica di questa roba qua sotto, c'è qualquadra che non cosa... :/
            while still_run:
                if coda_temp.empty() and len(delivering_list) == 0: #Se la coda è vuota aspetto che non diventi più vuota per aggiungere i messaggi in ddelivering_list
                    message_list = coda_temp.get()
                    delivering_list = delivering_list + message_list
                    #print("Ho appena ricevuto dei messaggini nuovi da spedirez! ^_^")
                else:
                    if not coda_temp.empty(): #Se mentre la coda non è vuota (ergo, mentre sis ta svuotando) si aggiunge 
                        message_list = coda_temp.get()
                        delivering_list = delivering_list + message_list
                        #print("Ho appena ricevuto dei messaggini nuovi da spedire! ^_^")
                    if len(delivering_list) > 0:
                        message = delivering_list.pop(0)
                        coda.put(message)






        if self.mode == "sender":
            while still_run:
                Sender.send_messages(coda, self.condizione)

                    



        if self.mode == "news_compiler":
            while still_run:
                with self.condizione:
                    self.condizione.wait()
                
                #TODO: Migliorare questa parte mettendo un tipo di messaggio a seconda del social
                messages_socials = []
                if subscriptions_dict:
                    for data_social in datas_social:
                        if data_social["social"] in subscriptions_dict["subscriptions"]:
                            if data_social["internal_id"] in subscriptions_dict["subscriptions"][data_social["social"]]:
                                for chat_id in subscriptions_dict["subscriptions"][data_social["social"]][data_social["internal_id"]]:
                                    message_title = data_social["title"]
                                    if data_social["type"] == "new_post":
                                        messages_socials.append({'type': 'sendMessage', 'text': "<b>["+data_social["social"].upper()+"]</b>\nUser: <i>"+message_title+"</i>\nLink: "+data_social["post_url"], 'chat_id': ''+str(chat_id)+'', 'markdown': 'HTML'})
                                    elif data_social["type"] == "now_private":
                                        messages_socials.append({'type': 'sendMessage', 'text': "<b>⚠️ALERT⚠️</b>\n<b>["+data_social["social"].upper()+"]</b>\nThis account now is <b>private</b>, that means that you'll no longer receive updates until its owner decides to change it back to <i>public</i>.\nUser: <i>"+message_title+"</i>\nLink: "+data_social["post_url"], 'chat_id': ''+str(chat_id)+'', 'markdown': 'HTML'})
                                    elif data_social["type"] == "deleted_account":
                                        messages_socials.append({'type': 'sendMessage', 'text': "<b>⚠️ALERT⚠️</b>\n<b>["+data_social["social"].upper()+"]</b>\nThis account has been <b>deleted</b> and also automatically removed from your <i>Follow List</i>.\nUser: <i>"+message_title+"</i>\nLink: "+data_social["post_url"], 'chat_id': ''+str(chat_id)+'', 'markdown': 'HTML'})
                                    else:
                                        messages_socials.append({'type': 'sendMessage', 'text': "<b>⚠️UNKNOWN MESSAGE⚠️</b>\nPlease report it to the creator of this bot.", 'chat_id': ''+str(chat_id)+'', 'markdown': 'HTML'})


                print("Messaggi da inviare: "+str(len(messages_socials)))
                if len(messages_socials) > 0:
                    coda_temp.put(messages_socials)
                    print("Messaggi messi in coda di spedizione.")




                
        if self.mode == "news_retreiver":
            global condizione_compiler
            while still_run:


                Database.clean_dead_subscriptions(database) #Pulisco al tabella "socials" rimuovendo gli account ai quali nessuno è più iscritto
                subscriptions_dict = Database.create_dict_of_userIds_and_socials(database) #Creo un dizionario di tutte le iscrizioni degli utenti
                #Creo un dizionario con tutti gli account a cui gli utenti sono iscritti
                socials_accounts_dict = {}
                #for social_key in subscriptions_dict.keys():
                #    socials_accounts_dict[social_key] = list(subscriptions_dict[social_key].keys())
                ######
                #print(subscriptions_dict["instagram"])


                if subscriptions_dict: #se il dizionario non è vuoto (la tabella "socials" non è vuota, ergo, almeno un utente è iscritto a qualcosa)


                    #In caso nessuno sia iscritto al social aggiungo un array ed un dizionario vuoti
                    num_subs_threads = {"subscriptions": {"total": 0}, "threads": {"total": 0}}
                    for element in ["instagram", "youtube", "flickr"]: #TODO: Portare fuori questa lista, che indica i servizi che sono abilitati per il retrieving delle informazioni
                        if element not in subscriptions_dict["subscriptions"]:
                            subscriptions_dict["subscriptions"][element] = {}
                        if element not in subscriptions_dict["social_accounts"]:
                            subscriptions_dict["social_accounts"][element] = []
                        num_subs_threads["subscriptions"][element] = len(subscriptions_dict["social_accounts"][element])
                        num_subs_threads["subscriptions"]["total"] = num_subs_threads["subscriptions"]["total"] + num_subs_threads["subscriptions"][element]
                        num_subs_threads["threads"][element] = 0

                    max_subscriptions_per_thread = 10 #Ricorda, deve essere assolutamente > 0
                    print("Account massimi per thread: "+str(max_subscriptions_per_thread))
                    for element in num_subs_threads["threads"]:
                        if element != "total":
                            num_subs_threads["threads"][element] = num_subs_threads["subscriptions"][element] // max_subscriptions_per_thread
                            if num_subs_threads["subscriptions"][element] % max_subscriptions_per_thread:
                                num_subs_threads["threads"][element] += 1
                            num_subs_threads["threads"]["total"] = num_subs_threads["threads"]["total"] + num_subs_threads["threads"][element]
                    print(num_subs_threads)



                    #Crea nuovi thread
                    coda_social = queue.Queue()
                    threads = []
                    threadID = 0
                    for tSocial,value in num_subs_threads["threads"].items():
                        if tSocial != "total":
                            head = 0
                            if max_subscriptions_per_thread > num_subs_threads["subscriptions"][tSocial]:
                                tail = num_subs_threads["subscriptions"][tSocial]
                            else:
                                tail = max_subscriptions_per_thread
                            for numThread in range(0,value):
                                
                                thread = threading.Thread(target=watchdog.news_retreiver_subthread, args=(threadID, tSocial, subscriptions_dict["social_accounts"][tSocial][head:tail], coda_social, apikeys))
                                thread.start()
                                threads.append(thread)
                                threadID += 1
                                head = tail
                                tail = tail + max_subscriptions_per_thread
                                if tail > num_subs_threads["subscriptions"][tSocial]:
                                    tail = num_subs_threads["subscriptions"][tSocial]


                    #Aspetta che tutti i thread finiscano
                    for t in threads:
                        t.join()

                    #Unisce i dati ricevuti dai thread
                    datas_social = []
                    datas_queries = {'update': [], 'delete': []}
                    while not coda_social.empty():
                        messaggio = coda_social.get()
                        datas_social = datas_social + messaggio["messages"]
                        datas_queries["update"] = datas_queries["update"] + messaggio["queries"]["update"]
                        datas_queries["delete"] = datas_queries["delete"] + messaggio["queries"]["delete"]


                    #Ordina i messaggi in ordine cronologico
                    if len(datas_social) > 1:
                        datas_social = sorted(datas_social, key=lambda k: k['post_date'], reverse = False)

                    #Credo che sarebbe meglio spostare questo coso qua sotto dopo la riga: print("Controllerò di nuovo tra 10 minuti.") o nel thread "News_compiler"
                    #Investigare a riguardo
                    Database.process_messages_queries(database, datas_queries)



                condizione_compiler.acquire()
                condizione_compiler.notify_all()
                condizione_compiler.release()
                print("Controllerò di nuovo tra 10 minuti.")





                with self.condizione:
                    self.condizione.wait(self.delay)






if __name__ == '__main__':
    main()

