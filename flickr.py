import time
import calendar
import datetime
import requests
from sender_receiver import Get_data

class Flickr:
    def flickr(social_accounts):

        messages = []
        queries = {}
        queries["update"] = []
        queries["delete"] = []

        for value in social_accounts:

            userId = value["internal_id"]
            user = value["username"]
            title = value["title"]
            status = value["status"]

            print("Getting JSON from Flickr of "+value["username"]+"...")
            f = Get_data.get_json_from_url("http://api.flickr.com/services/feeds/photos_public.gne?id="+userId+"&format=json", "flickr-internal_id")

            request_status = True
            if "error" in f: #Controllo che non ci siano errori come account non esistente (o cancellato) o chiave non valida
                reason = f["reason"]
                request_status = False
            elif "items" in f: #Controllo che il canale non sia vuoto
                if not f["items"]:
                    reason = "emptyChannel"
                    request_status = False

            if request_status: #Se l'account esiste e non è vuoto

                item_lastDate = int(value["retreive_time"])#-17280000

                item_lastDate_Temp = item_lastDate
                for post in f["items"]:
                    item_title = post["title"]
                    item_description = post["description"]
                    item_url = post["link"]
                    item_date = int(calendar.timegm(datetime.datetime.strptime(post["published"], "%Y-%m-%dT%H:%M:%SZ").timetuple()))
                    if item_date > item_lastDate:
                        messages.append({"type": "new_post", "social": "flickr", "internal_id": userId, "username": user, "title": title, "post_title": item_title, "post_description": item_description, "post_url": item_url, "media_source": None, "post_date": item_date})
                        if item_date > item_lastDate_Temp:
                            item_lastDate_Temp = item_date

                item_lastDate = item_lastDate_Temp

                queries["update"].append({'type':'retreive_time', 'social':'flickr', 'internal_id': userId, 'retreive_time': str(item_lastDate)})

            else:
                if reason == "userNotFound":
                    #Se l'account non esiste più allora lo cancello
                    print("Il profilo "+user+" non esiste più, ora lo cancello.")
                    queries["delete"].append({'type':'socialAccount', 'social':'flickr', 'internal_id': userId})
                    messages.append({"type": "deleted_account" ,"social": "flickr", "internal_id": userId, "username": user, 'title': title, "post_url": "https://www.flickr.com/people/"+user, "post_date": int(time.time())})
                elif reason == "emptyChannel":
                    pass
                else:
                    pass #Oltrepassato il rate limit od errore sconosciuto


        #Messaggi ordinati cronologicamente
        messages.reverse()
        
        return {'messages': messages, 'queries': queries}


